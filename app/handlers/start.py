from aiogram import types

from app import text
from app.common import FMT


async def cmd_start(message: types.Message, f: FMT):
    if not await f.db.is_registered(message.from_user.id):
        await f.db.register(message.from_user.id)

    await message.reply(text.cmd_start)


def register(dp):
    dp.register_message_handler(cmd_start, commands="start")
