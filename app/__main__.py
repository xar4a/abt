"""Application running"""
import asyncio
import logging
import argparse

from aiogram import Bot, Dispatcher
from aiogram.bot.api import TelegramAPIServer
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.types import BotCommand
from aiogram.types.bot_command_scope import BotCommandScopeDefault

from app import db
from app.config_parser import parse_config
from app.utils import get_handled_updates_list


def parse_arguments():
    parser = argparse.ArgumentParser(description="Process bot configuration.")
    parser.add_argument("--config", "-c", type=str, help="configuration file", default="config.toml")

    return parser.parse_args()


async def set_bot_commands(bot: Bot):
    commands = [
        BotCommand(command="start", description="Just start")
    ]
    await bot.set_my_commands(commands, scope=BotCommandScopeDefault())


def register_all(dp, config, session_maker):
    from app import filters
    filters.register(dp, config)

    from app import handlers
    handlers.register(dp)

    from app import middlewares
    middlewares.register(dp, config, session_maker)


async def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s|%(levelname)s|%(name)s|%(message)s",
        datefmt='%Y-%m-%d|%H:%M:%S',
    )

    arguments = parse_arguments()
    config = parse_config(arguments.config)

    async_sessionmanager = await db.init(config.engine)

    api_server = TelegramAPIServer.from_base(config.api)
    bot = Bot(config.token, parse_mode="HTML", server=api_server)
    storage = MemoryStorage()  # TODO: add option to config
    dp = Dispatcher(bot, storage=storage)

    register_all(dp, config, async_sessionmanager)

    await set_bot_commands(bot)

    try:  # TODO: add polling
        await dp.start_polling(allowed_updates=get_handled_updates_list(dp))
    finally:
        await dp.storage.close()
        await dp.storage.wait_closed()
        session = await bot.get_session()
        if session:
            await session.close()

try:
    asyncio.run(main())
except (KeyboardInterrupt, SystemExit):
    logging.error("Bot stopped!")
