import inspect

from aiogram import types
from aiogram.dispatcher.handler import current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware

from app.common import FMT


class DBSessionMiddleware(BaseMiddleware):
    """
    Simple middleware
    """

    def __init__(self, config, session_maker):
        self.config = config
        self.session_maker = session_maker
        super().__init__()

    async def on_process_message(self, message: types.Message, data: dict):
        handler = current_handler.get()
        args = inspect.getfullargspec(handler)[0]

        if "f" in args:
            async with self.session_maker() as session:
                data["f"] = FMT(db=session, config=self.config)
        elif "ff" in args:
            data["ff"] = FMT(db=None, config=self.config)

        return True

    async def on_post_process_message(self, message: types.Message, results, data: dict):
        if "f" in data:
            f: FMT = data["f"]
            await f.db.close()

        return True


def register(dp, config, session_maker):
    dp.middleware.setup(DBSessionMiddleware(config, session_maker))
