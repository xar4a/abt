from aiogram import Dispatcher, types
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.utils import exceptions
from aiogram.utils.exceptions import Throttled

from app import text


class ThrottlingMiddleware(BaseMiddleware):
    """
    Simple middleware
    """

    def __init__(self, config, limit=2, key="antiflood__message"):
        self.config = config
        self.limit = limit
        self.key = key
        super(ThrottlingMiddleware, self).__init__()

    async def on_process_message(self, message: types.Message, data: dict):
        """
        This handler is called when dispatcher receives a message
        :param data:
        :param message:
        """
        if message.from_user.id in self.config.admins:
            return

        dispatcher = Dispatcher.get_current()
        try:
            await dispatcher.throttle(self.key, rate=self.limit)
        except Throttled as t:
            if t.exceeded_count <= 2:
                try:
                    await message.answer(text.antiflood_warn)
                except exceptions.BadRequest:
                    pass

            raise CancelHandler()


def register(dp, config):
    dp.middleware.setup(ThrottlingMiddleware(config))
