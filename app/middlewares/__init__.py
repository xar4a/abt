def register(dp, config, session_maker):
    from . import throttling
    from . import f
    middlewares_list = [
        throttling
    ]
    for i in middlewares_list:
        i.register(dp, config)
    f.register(dp, config, session_maker)
