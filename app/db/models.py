import time
from secrets import token_urlsafe

from sqlalchemy import Column, BigInteger, Text, ForeignKey, Float, Integer
from sqlalchemy.orm import relationship

from app.db.base import Base


class User(Base):
    __tablename__ = "users"

    id = Column(BigInteger, primary_key=True, unique=True, autoincrement=False)
    last_message = Column(Float, autoincrement=False, default=lambda: int(time.time()))
