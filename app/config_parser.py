import os
from dataclasses import dataclass
from typing import List

import toml


@dataclass
class Config:
    token: str
    api: str
    admins: List[int]

    engine: str


config: Config


def parse_config(config_file: str) -> Config:
    # TODO: add an automatic parsing with dataclasses.fields
    if not os.path.isfile(config_file) and not config_file.endswith(".toml"):
        config_file += ".toml"

    if not os.path.isfile(config_file):
        raise FileNotFoundError(f"Config file not found: {config_file} no such file")

    with open(config_file, "r") as f:
        data = toml.load(f)

    return Config(
        token=data["bot"]["token"],
        api=data["bot"].get("api", "https://api.telegram.org/"),
        admins=data["bot"]["admins"],

        engine=data["database"]["engine"],
    )
