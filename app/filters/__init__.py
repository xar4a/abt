def register(dp, config):
    from . import is_admin
    filters_list = [
        is_admin
    ]
    for i in filters_list:
        i.register(dp, config)
