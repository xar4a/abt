# Aiogram Bot Template
My Default template for telegram bots written with aiogram

# How to run
```bash
pip3 install -r requerments.txt
python3 -m app -c config.toml
```

[comment]: <> (## Use webhook)

[comment]: <> (Add [webhook] to config file, and set up fields:)

[comment]: <> ( * host)

[comment]: <> ( * port)

[comment]: <> ( * url_path)

[comment]: <> ( * protocol)

[comment]: <> ( * certificate)

[comment]: <> ( * private_key)

## Local bot-API
Add api field to [bot] with local bot-API, default is https://api.telegram.org
